#!/usr/bin/env bash

# This script creates binaries and docker images with the given name, e.g. "alpha", "beta", "production".
# It does not rebuild, it copies the artifacts created by the build.sh script

# fail immediately
set -e
set -o pipefail

if [ "$#" -ne 1 ]; then
    echo "missing environment parameter"
    echo "Usage: deploy.sh <environment>"
    echo "<environment> can be something like \"alpha\", \"beta\", \"production\""
    echo "and will be used as part of the binary name and as the docker image tag."
    exit 1
fi

. prepare.sh

ENV="$1"

if [ "${BB_AUTH_STRING}" ]; then

    # get hashed binaries, rename, upload
    URL="https://bitbucket.org/amdatulabs/amdatu-kubernetes-deployer/downloads"
    wget "${URL}/${LINUX_NAME_HASHED}"
    wget "${URL}/${MACOS_NAME_HASHED}"
    wget "${URL}/${WIN_NAME_HASHED}"

    LINUX_NAME_ENV="${LINUX_NAME_BASE}-${ENV}"
    MACOS_NAME_ENV="${MACOS_NAME_BASE}-${ENV}"
    WIN_NAME_ENV="${WIN_NAME_BASE}-${ENV}.exe"

    mv "${LINUX_NAME_HASHED}" "${LINUX_NAME_ENV}"
    mv "${MACOS_NAME_HASHED}" "${MACOS_NAME_ENV}"
    mv "${WIN_NAME_HASHED}" "${WIN_NAME_ENV}"

    echo "Uploading binaries"

    curl -X POST "${UPLOAD_URL}" --form files=@"${LINUX_NAME_ENV}"
    curl -X POST "${UPLOAD_URL}" --form files=@"${MACOS_NAME_ENV}"
    curl -X POST "${UPLOAD_URL}" --form files=@"${WIN_NAME_ENV}"
else
    echo "No bitbucket auth token set, skipping upload"
fi

if [ "${DOCKER_USER}" ] && [ "${DOCKER_PASSWORD}" ]; then
    # pull hashed image, retag, push
    echo "Pulling hashed image"
    docker login --username="${DOCKER_USER}" --password="${DOCKER_PASSWORD}"
    IMAGE="${IMAGE_NAME}:${HASH}"
    docker pull "${IMAGE}"
    echo "Tagging new image"
    IMAGE_ENV="${IMAGE_NAME}:${ENV}"
    docker tag "${IMAGE}" "${IMAGE_ENV}"
    echo "Pushing docker image"
    docker push "${IMAGE_ENV}"

    if [ "${ENV}" == "production" ]; then
        echo "also tagging and pushing latest image"
        IMAGE_LATEST="${IMAGE_NAME}:latest"
        docker tag "${IMAGE}" "${IMAGE_LATEST}"
        docker push "${IMAGE_LATEST}"
    fi

else
    echo "No docker user/password set, skipping image build and push"
fi

echo "Done"
