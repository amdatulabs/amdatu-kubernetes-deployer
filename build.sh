#!/usr/bin/env bash

# This script builds binaries and the docker image with names/tag based on the git commit hash

# fail immediately
set -e
set -o pipefail

. prepare.sh

# save where we are coming from, that's where our sources are
SRCDIR=`pwd`

# create correct golang dir structure for this project
echo "Creating directory structure"
PACKAGE_PATH="${GOPATH}/src/bitbucket.org/amdatulabs/${APPNAME}"
mkdir -p "${PACKAGE_PATH}"

# copy sources to new directory
echo "Copying sources"
cd "${SRCDIR}"
tar -cO --exclude .git . | tar -xv -C "${PACKAGE_PATH}"

# build binaries
echo "Building binaries"
cd "${PACKAGE_PATH}"

CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o "${LINUX_NAME_HASHED}" .
CGO_ENABLED=0 GOOS=darwin go build -a -installsuffix cgo -o "${MACOS_NAME_HASHED}" .
CGO_ENABLED=0 GOOS=windows go build -a -installsuffix cgo -o "${WIN_NAME_HASHED}" .

if [ "$BB_AUTH_STRING" ]; then
    # upload binaries to download section of bitbucket
    echo "Uploading binaries"
    curl -X POST "${UPLOAD_URL}" --form files=@"${LINUX_NAME_HASHED}"
    curl -X POST "${UPLOAD_URL}" --form files=@"${MACOS_NAME_HASHED}"
    curl -X POST "${UPLOAD_URL}" --form files=@"${WIN_NAME_HASHED}"
else
    echo "No bitbucket auth token set, skipping upload"
fi

if [ "${DOCKER_USER}" ] && [ "${DOCKER_PASSWORD}" ]; then
    # build docker image
    echo "Building docker image"
    cp "${LINUX_NAME_HASHED}" "${APPNAME}"
    IMAGE="${IMAGE_NAME}:${HASH}"
    docker build -t "${IMAGE}" .
    echo "Pushing docker image"
    docker login --username="${DOCKER_USER}" --password="${DOCKER_PASSWORD}"
    docker push "${IMAGE}"
else
    echo "No docker user/password set, skipping image build and push"
fi

echo "Done"
