#!/usr/bin/env bash

# Source this script for getting some constants into both build.sh and deploy.sh

# fail immediately
set -e
set -o pipefail

echo "Branch: $BITBUCKET_BRANCH"
echo "Tag: $BITBUCKET_TAG"
echo "Commit: $BITBUCKET_COMMIT"

APPNAME="amdatu-kubernetes-deployer"

LINUX_NAME_BASE="${APPNAME}-linux_amd64"
MACOS_NAME_BASE="${APPNAME}-macos_amd64"
WIN_NAME_BASE="${APPNAME}-windows_amd64"

HASH="${BITBUCKET_COMMIT}"
LINUX_NAME_HASHED="${LINUX_NAME_BASE}-${HASH}"
MACOS_NAME_HASHED="${MACOS_NAME_BASE}-${HASH}"
WIN_NAME_HASHED="${WIN_NAME_BASE}-${HASH}.exe"

IMAGE_NAME="amdatu/${APPNAME}"

UPLOAD_URL="https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads"
