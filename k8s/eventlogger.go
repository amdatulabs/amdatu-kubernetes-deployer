package k8s

import (
	"bitbucket.org/amdatulabs/amdatu-kubernetes-deployer/logger"
	"k8s.io/client-go/pkg/api/v1"
)

type EventLogger struct {
	namespace string
	appname   string
	logger    logger.Logger
	k8sClient *K8sClient
	stopped   chan bool
}

func NewEventLogger(namespace string, appname string, logger logger.Logger, k8sClient *K8sClient) EventLogger {
	return EventLogger{
		namespace: namespace,
		appname:   appname,
		logger:    logger,
		k8sClient: k8sClient,
		stopped:   make(chan bool),
	}
}

func (ev EventLogger) Start() error {

	watch, err := ev.k8sClient.WatchEvents(ev.namespace)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ev.stopped:
			// we were stopped
			watch.Stop()
			return nil
		case watchEvent := <-watch.ResultChan():
			// process event
			if event, ok := watchEvent.Object.(*v1.Event); ok {
				ev.logger.Printf("[k8s] %v %v: %v", event.InvolvedObject.Kind, event.InvolvedObject.Name, event.Message)
			}
		}
	}

}

func (ev EventLogger) Stop() {
	ev.stopped <- true
	close(ev.stopped)
}
